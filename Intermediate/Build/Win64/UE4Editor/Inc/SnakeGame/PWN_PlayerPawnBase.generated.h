// Copyright Epic Games, Inc. All Rights Reserved.
/*===========================================================================
	Generated code exported from UnrealHeaderTool.
	DO NOT modify this manually! Edit the corresponding .h files instead!
===========================================================================*/

#include "UObject/ObjectMacros.h"
#include "UObject/ScriptMacros.h"

PRAGMA_DISABLE_DEPRECATION_WARNINGS
#ifdef SNAKEGAME_PWN_PlayerPawnBase_generated_h
#error "PWN_PlayerPawnBase.generated.h already included, missing '#pragma once' in PWN_PlayerPawnBase.h"
#endif
#define SNAKEGAME_PWN_PlayerPawnBase_generated_h

#define Homework20_Source_SnakeGame_PWN_PlayerPawnBase_h_15_SPARSE_DATA
#define Homework20_Source_SnakeGame_PWN_PlayerPawnBase_h_15_RPC_WRAPPERS \
 \
	DECLARE_FUNCTION(execHandlerPlayerHorizontalInput); \
	DECLARE_FUNCTION(execHandlerPlayerVerticalInput);


#define Homework20_Source_SnakeGame_PWN_PlayerPawnBase_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
 \
	DECLARE_FUNCTION(execHandlerPlayerHorizontalInput); \
	DECLARE_FUNCTION(execHandlerPlayerVerticalInput);


#define Homework20_Source_SnakeGame_PWN_PlayerPawnBase_h_15_INCLASS_NO_PURE_DECLS \
private: \
	static void StaticRegisterNativesAPWN_PlayerPawnBase(); \
	friend struct Z_Construct_UClass_APWN_PlayerPawnBase_Statics; \
public: \
	DECLARE_CLASS(APWN_PlayerPawnBase, APawn, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/SnakeGame"), NO_API) \
	DECLARE_SERIALIZER(APWN_PlayerPawnBase)


#define Homework20_Source_SnakeGame_PWN_PlayerPawnBase_h_15_INCLASS \
private: \
	static void StaticRegisterNativesAPWN_PlayerPawnBase(); \
	friend struct Z_Construct_UClass_APWN_PlayerPawnBase_Statics; \
public: \
	DECLARE_CLASS(APWN_PlayerPawnBase, APawn, COMPILED_IN_FLAGS(0 | CLASS_Config), CASTCLASS_None, TEXT("/Script/SnakeGame"), NO_API) \
	DECLARE_SERIALIZER(APWN_PlayerPawnBase)


#define Homework20_Source_SnakeGame_PWN_PlayerPawnBase_h_15_STANDARD_CONSTRUCTORS \
	/** Standard constructor, called after all reflected properties have been initialized */ \
	NO_API APWN_PlayerPawnBase(const FObjectInitializer& ObjectInitializer); \
	DEFINE_DEFAULT_OBJECT_INITIALIZER_CONSTRUCTOR_CALL(APWN_PlayerPawnBase) \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, APWN_PlayerPawnBase); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(APWN_PlayerPawnBase); \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API APWN_PlayerPawnBase(APWN_PlayerPawnBase&&); \
	NO_API APWN_PlayerPawnBase(const APWN_PlayerPawnBase&); \
public:


#define Homework20_Source_SnakeGame_PWN_PlayerPawnBase_h_15_ENHANCED_CONSTRUCTORS \
private: \
	/** Private move- and copy-constructors, should never be used */ \
	NO_API APWN_PlayerPawnBase(APWN_PlayerPawnBase&&); \
	NO_API APWN_PlayerPawnBase(const APWN_PlayerPawnBase&); \
public: \
	DECLARE_VTABLE_PTR_HELPER_CTOR(NO_API, APWN_PlayerPawnBase); \
	DEFINE_VTABLE_PTR_HELPER_CTOR_CALLER(APWN_PlayerPawnBase); \
	DEFINE_DEFAULT_CONSTRUCTOR_CALL(APWN_PlayerPawnBase)


#define Homework20_Source_SnakeGame_PWN_PlayerPawnBase_h_15_PRIVATE_PROPERTY_OFFSET
#define Homework20_Source_SnakeGame_PWN_PlayerPawnBase_h_12_PROLOG
#define Homework20_Source_SnakeGame_PWN_PlayerPawnBase_h_15_GENERATED_BODY_LEGACY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Homework20_Source_SnakeGame_PWN_PlayerPawnBase_h_15_PRIVATE_PROPERTY_OFFSET \
	Homework20_Source_SnakeGame_PWN_PlayerPawnBase_h_15_SPARSE_DATA \
	Homework20_Source_SnakeGame_PWN_PlayerPawnBase_h_15_RPC_WRAPPERS \
	Homework20_Source_SnakeGame_PWN_PlayerPawnBase_h_15_INCLASS \
	Homework20_Source_SnakeGame_PWN_PlayerPawnBase_h_15_STANDARD_CONSTRUCTORS \
public: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


#define Homework20_Source_SnakeGame_PWN_PlayerPawnBase_h_15_GENERATED_BODY \
PRAGMA_DISABLE_DEPRECATION_WARNINGS \
public: \
	Homework20_Source_SnakeGame_PWN_PlayerPawnBase_h_15_PRIVATE_PROPERTY_OFFSET \
	Homework20_Source_SnakeGame_PWN_PlayerPawnBase_h_15_SPARSE_DATA \
	Homework20_Source_SnakeGame_PWN_PlayerPawnBase_h_15_RPC_WRAPPERS_NO_PURE_DECLS \
	Homework20_Source_SnakeGame_PWN_PlayerPawnBase_h_15_INCLASS_NO_PURE_DECLS \
	Homework20_Source_SnakeGame_PWN_PlayerPawnBase_h_15_ENHANCED_CONSTRUCTORS \
private: \
PRAGMA_ENABLE_DEPRECATION_WARNINGS


template<> SNAKEGAME_API UClass* StaticClass<class APWN_PlayerPawnBase>();

#undef CURRENT_FILE_ID
#define CURRENT_FILE_ID Homework20_Source_SnakeGame_PWN_PlayerPawnBase_h


PRAGMA_ENABLE_DEPRECATION_WARNINGS
